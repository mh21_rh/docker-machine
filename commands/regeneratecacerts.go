package commands

import (
	"os"

	"github.com/docker/machine/commands/mcndirs"
	"github.com/docker/machine/libmachine"
	"github.com/docker/machine/libmachine/auth"
	"github.com/docker/machine/libmachine/cert"
	"github.com/docker/machine/libmachine/log"
)

func cmdRegenerateCACerts(c CommandLine, api libmachine.API) error {
	log.Infof("Regenerating CA TLS certificates")

	// copied from commands/create.go
	authOptions := &auth.Options{
		CertDir:          mcndirs.GetMachineCertDir(),
		CaCertPath:       tlsPath(c, "tls-ca-cert", "ca.pem"),
		CaPrivateKeyPath: tlsPath(c, "tls-ca-key", "ca-key.pem"),
		ClientCertPath:   tlsPath(c, "tls-client-cert", "cert.pem"),
		ClientKeyPath:    tlsPath(c, "tls-client-key", "key.pem"),
	}

	for _, filePath := range []string{
		authOptions.CaCertPath,
		authOptions.CaPrivateKeyPath,
		authOptions.ClientCertPath,
		authOptions.ClientKeyPath,
	} {
		if _, err := os.Stat(filePath); err == nil {
			if err := os.Remove(filePath); err != nil {
				return err
			}
		}
	}

	if err := cert.BootstrapCertificates(authOptions); err != nil {
		return err
	}

	return nil
}
